from threading import Thread


class BotThread(Thread):

    def __init__(self, func):
        """Инициализация потока"""
        Thread.__init__(self)
        self.func = func

    def run(self):
        self.func()
