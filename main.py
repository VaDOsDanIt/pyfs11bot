from flask import Flask, request
from bot import bot
from Thread import BotThread
from keyboards import get_url_keyboard
from config import GROUP_CHAT_ID
import json
import os

app = Flask(__name__)


@app.route('/ping', methods=['GET'])
def ping():
    return "pong"


@app.route('/notify', methods=['POST'])
def notify():
    webhook_json = json.loads(request.get_data())
    pull_request_json = webhook_json['pull_request']

    action = webhook_json['action']
    url = pull_request_json['html_url']
    title = pull_request_json['title']
    src = pull_request_json['head']['ref']
    dest = pull_request_json['base']['ref']
    login = pull_request_json['user']['login']

    print(webhook_json)

    response = str(f"Pull request: \n"
                   f"{src} -> {dest}\n"
                   f"Действие: {action}\n"
                   f"Название: {title}\n"
                   f"Разработчик: {login}\n"
                   f"URL: {url}")

    bot.send_message(chat_id=GROUP_CHAT_ID,
                     text=response,
                     reply_markup=get_url_keyboard(url))

    return response


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 33507))

    thread1 = BotThread(bot.infinity_polling)
    app.run(port=port)
    thread1.start()
