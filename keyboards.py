from telebot import types


def get_url_keyboard(url):
    url_keyboard = types.InlineKeyboardMarkup(row_width=3)

    btn = types.InlineKeyboardButton(text="Открыть", url=url)

    url_keyboard.add(btn)
    return url_keyboard
